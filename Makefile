CC=g++
CXXFLAGS=-std=c++17 -Wall -ggdb
LDFLAGS=
SOURCES=finder.cc
OBJECTS=$(SOURCES:.cc=.o)
EXECUTABLE=finder

all: $(SOURCES) $(EXECUTABLE)

$(EXECUTABLE): $(OBJECTS)
	$(CC) $(LDFLAGS) $(OBJECTS) -o $@

.cpp.o:
	$(CC) $(CXXFLAGS) $< -o $@

clean:
	rm -v *.o $(EXECUTABLE)
