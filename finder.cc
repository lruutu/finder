#include <iostream>
#include <list>
#include <string>
#include <dirent.h>
#include <unistd.h>

// finder <pattern> <directory>
int main(int argc, char **argv) {
    bool verbose = false;
    bool breadth_first = false;
    int opt;

    while ((opt = getopt(argc, argv, "bv")) != -1) {
        switch (opt) {
            case 'v':
                verbose = true;
                break;
            case 'b':
                breadth_first = true;
                break;
        }
    }

    if ((argc - optind) < 2) {
        std::cerr << "Not enough arguments!" << std::endl <<
            "Usage: finder [-v] <pattern> <directory>" << std::endl <<
            " -v: enable verbose output" << std::endl <<
            " -b: search breadth-first" << std::endl;
        return 1;
    }

    char *pattern = argv[optind];
    char *start_dir = argv[optind + 1];

    std::list<std::string> directories;
    directories.push_back(start_dir);

    while (directories.size() > 0) {
        // read beginning of directories, check for pattern, add newly found dirs to end
        std::string current = directories.front();
        if (current != "/") {
            current += "/";
        }
        directories.pop_front();

        errno = 0;
        DIR *dir_stream = opendir(current.c_str());
        // Error check directory opening
        if (dir_stream == nullptr) {
            std::string err_msg;
            switch (errno) {
                case EACCES:
                    err_msg = "ERROR: read permission denied";
                    break;
                case ENAMETOOLONG:
                    err_msg = "ERROR: directory name too long";
                    break;
                case ENOENT:
                    err_msg = "ERROR: dirname does not name an existing directory or dirname is empty";
                    break;
                case ENOTDIR:
                    err_msg = "ERROR: dirname is not a directory";
                    break;
            }
            std::cerr << err_msg << '\t' << current << std::endl;
            continue;
        }

        dirent *entry;
        std::list<std::string> subdirectories;

        // Loop through entries in directory and get subdirectories
        while ((entry = readdir(dir_stream)) != nullptr) {
            // Ignore dot and dot-dot directories
            if ((std::string(".").compare(entry->d_name) == 0)
             || (std::string("..").compare(entry->d_name) == 0)) {
                continue;
            }

            if (entry->d_type == DT_DIR) {
                subdirectories.push_back(current + entry->d_name); 
            }

            if (std::string(entry->d_name).find(pattern) != std::string::npos) {
                if (verbose) {
                    std::string file_type;
                    switch (entry->d_type) {
                        case DT_DIR:
                            file_type = "\033[32mdirectory";
                            break;
                        case DT_LNK:
                            file_type = "\033[34msymbolic link";
                            break;
                        case DT_REG:
                            file_type = "\033[33mfile";
                            break;
                        case DT_FIFO:
                            file_type = "\033[36mnamed pipe/FIFO";
                            break;
                        case DT_BLK:
                            file_type = "\033[35mblock device";
                            break;
                        case DT_SOCK:
                            file_type = "\033[1;31msocket";
                            break;
                        case DT_CHR:
                            file_type = "\033[31mcharacter device";
                            break;
                    }

                    std::cout << file_type << "\033[0m \033[1;37m" << entry->d_name << "\033[0m at " << current << entry->d_name << std::endl;
                }
                else {
                    std::cout << current << entry->d_name << std::endl;
                }
            }
        }

        subdirectories.sort();

        if (breadth_first) {
            directories.splice(directories.end(), subdirectories);
        }
        else {
            directories.splice(directories.begin(), subdirectories);
        }

        errno = 0;
        if (closedir(dir_stream) != 0) {
            switch (errno) {
                case EBADF:
                    std::cerr << "ERROR: directory pointer does not refer to an open directory stream" << std::endl;
                    break;
                case EINTR:
                    std::cerr << "ERROR: closedir() interrupted by a signal" << std::endl;
                    break;
            }
        }
    }

    return 0;
}
